#!/bin/env python
import argparse
import os

def main():
  import ROOT

  # define process identifier, input file and observable
  identifier = "ggfZZ" # avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  infilename = "input/ggfhzz4l_2d.root" # give the input file name here
  observable = "base/phi" # name of the observable to be used (histogram name)

  # these are the names of the input samples
  samplelist = ["s1","s2","s3"]
  # these are the validation samples: "v0","v1"

  # push all the input samples in a RooArgList
  inputs = ROOT.RooArgList()
  # we need the additional list "inputnames" to prevent the python garbage collector from deleting the RooStringVars
  inputnames = []
  for sample in samplelist:
    v = ROOT.RooStringVar(sample,sample,sample)
    inputnames.append(v)
    inputs.add(v)

  # setup predefined morphfunc by hand
  morphfunc = ROOT.RooHCggfZZMorphFunc(identifier,identifier,infilename,observable,inputs)

  # morph to the validation sample v1
  validationsample = "v1"
  morphfunc.setParameters(validationsample)
  morphing = morphfunc.createTH1("morphing")

  # open the input file to get the validation histogram for comparison
  tfile = ROOT.TFile.Open(infilename,"READ")
  folder = tfile.Get(validationsample)
  validation = folder.FindObject(observable)
  validation.SetDirectory(0)
  validation.SetTitle(validationsample)
  tfile.Close()

  # plot everything
  plot = ROOT.TCanvas("plot")
  plot.cd()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetOptTitle(0)
  morphing.GetXaxis().SetTitle(observable)
  morphing.SetLineColor(ROOT.kRed)
  morphing.SetFillColor(ROOT.kRed)
  morphing.Draw("E3")
  validation.Draw("SAME")
  leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
  leg.AddEntry(morphing)
  leg.AddEntry(validation)
  leg.Draw()
  plot.SaveAs("plot.pdf","pdf")

if __name__ == "__main__":
  # some argument parsing to provide the path to the RooLagrangianMorphFunc
  parser = argparse.ArgumentParser("morphing example")
  args = parser.parse_args()
  # load all required libraries
  from ROOT import gSystem
  gSystem.Load("libRooFit")
  gSystem.Load("libRooLagrangianMorphing.so")
  # call the main function
  main()
