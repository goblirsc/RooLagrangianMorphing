// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooStringVar.h"

// ROOT
#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TFolder.h"
#include "TLegend.h"
#include "TStyle.h"

int main(int argc, const char* argv[]){

  // define process identifier, input file and observable
  std::string identifier("ggfZZ"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/ggfhzz4l_2d.root"); // give the input file name here
  std::string observable("base/phi"); // name of the observable to be used (histogram name)

  // these are the names of the input samples
  std::vector<std::string> samplelist = {"s1","s2","s3"};
  // these are the validation samples: "v0","v1"

  // push all the input samples in a RooArgList
  RooArgList inputs;
  for(auto const& sample: samplelist) {
    RooStringVar* v = new RooStringVar(sample.c_str(),sample.c_str(),sample.c_str());
    inputs.add(*v);
  }

  // setup predefined morphfunc by hand
  RooLagrangianMorphFunc* morphfunc = new RooHCggfZZMorphFunc(identifier.c_str(),identifier.c_str(),infilename.c_str(),observable.c_str(),inputs);

  // morph to the validation sample v1
  std::string validationsample("v1");
  morphfunc->setParameters(validationsample.c_str());
  TH1* morphing = morphfunc->createTH1("morphing");

  // open the input file to get the validation histogram for comparison
  TFile* file = TFile::Open(infilename.c_str(),"READ");
  TFolder* folder = 0;
  file->GetObject(validationsample.c_str(),folder);
  TH1* validation = dynamic_cast<TH1*>(folder->FindObject(observable.c_str()));
  validation->SetDirectory(NULL);
  validation->SetTitle(validationsample.c_str());
  file->Close();

  // plot everything
  TCanvas* plot = new TCanvas("plot");
  plot->cd();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  morphing->GetXaxis()->SetTitle(observable.c_str());
  morphing->SetLineColor(kRed);
  morphing->SetFillColor(kRed);
  morphing->Draw("E3");
  validation->Draw("SAME");
  TLegend* leg = new TLegend(0.7,0.7,0.9,0.9);
  leg->AddEntry(morphing);
  leg->AddEntry(validation);
  leg->Draw();
  plot->SaveAs("plot.pdf","pdf");

  return 0;
}
