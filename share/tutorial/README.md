Introduction
===

This page provides tutorial instructions for the RooLagrangianMorphing <a href="https://indico.cern.ch/event/507948/" target="_blank" title="Indico Page of Tutorial Workshop">Tutorial</a>. It is intended as a hands-on, step-by-step instruction walk-through that will guide the interested user through the usage of that tool.

Lessons
===

This section contains individual "lessons" - small code snippets that allow the user to get familiar with the RooLagrangianMorphing by teaching the usage of individual features step-by-step.

For the purposes of this tutorial, we assume that you are working via a remote shell on an lxplus login node.

Each lesson contains a section entitled "Playground", that will provide some trivia and provide you with questions and challenges that may spark your curiosity and provide you with an easy starting point to play with the code!

Lesson 0: Setting up the code
---

First, as for most ROOT-based tools, you need to setup a compatible version of ROOT. Any version of ROOT 6.14 or later will suffice.

You will also need to download the input files used in this tutorial. For this purpose, execute the `download.sh` script located in the `lesson0` folder, which will use `wget` to download the files for you.

### Playground
   * Fire up a ROOT shell and make sure that the ROOT setup we provided works as inteded by executing a few basic tasks: Simple math, creating a histogram, whatever comes to your mind.
   * Open one of the input files provided above and explore their contents using the TBrowser. They contain a folder structure of several input samples, each one with the same set of physics distributions inside. Each input sample also contains a histogram named "param_card", which contains the values of the EFT parameters used for the production of that particular sample. Look at the histograms and familiarize yourself with the file structure.

Lesson 1: Simple Morphing with 2 Parameters
---

In order to get to grips with the RooLagrangianMorphing, this <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson1" target="_blank" title="Lesson 1">link</a> will lead you to a web directory that contains a simplistic code examples (both in python and C++) that show the usage of the tool. Take a moment to look through the code and try to understand the individual steps!

For the course of this tutorial, we assume that you create a working directory, with a subfolder for each individual lesson. For example, logged in at lxplus type in the following:

    mkdir RooLagrangianMorphingTutorial
    cd RooLagrangianMorphingTutorial
    mkdir Lesson1


The histograms we will use as an input are located in a ROOT file. The name of that file is stored in the variable called "infilename". Since that file contains multiple histograms, the exact histogram we will be using is identified by the variable "observable". You can open the input file and explore it's hierarchy to use a different input distribution if you like to!

Since the input file contains input from a lot of different samples, we need to tell the RooLagrangianMorphing which ones to use as a basis for the morphing. This is done by pushing their names into a list.

With the name of the input file and the list of input sample names, we can call the constructor of the RooLagrangianMorphing to obtain an instance. Since the input we are using is a sample of ggF Higgs production with decay to two Z bosons, we are using the corresponding variant of the RooLagrangianMorphing called "RooHCggfZZMorphFunc". You will later learn how to create your own variants of this class for different physical processes. The identifier handed to the constructor is just a name that will help you identify the object when you store it in a workspace, which will you will be doing shortly.

Next, we want to see that the morphing actually works. Hence, we set the parameters to one of the validation samples that are available in the same input file. Conveniently, the input format used by the RooLagrangianMorphing also contains all the parameters, such that we only need to call the method "setParameters" with the name of a sample as an argument to use its parameter settings. Using the "createTH1" method, we create a histogram with the morphed distribution predicted at the given parameters by the RooLagrangianMorphing. The argument given to the method is again just an arbitrary object name.

In order to have something to compare to, we also retrieve the corresponding histogram of the validation sample from the same input file.

At the end, we create a canvas and a legend in order to plot both histograms - the one obtained from the validation sample and the morphing prediction for the same parameter set - for comparison.


For the rest of this tutorial, this script will be successively expanded in individual lessons to allow you to explore a good fraction of the features supplied by the RooLagrangianMorphing. You can choose whether you prefer following this tutorial using C++ or python, but it's probably a good idea to stick to your choice once you have made a decision. If you choose to follow the C++ branch of the tutorial, you will probably be thankful for a <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/compile" target="_blank" title="compile">compilation script</a> that will save you from having to worry about the required flags for compilation and linkage. In order to use it to compile a source file "source/file.cxx" into an executable "source/file" while linking against a version of RooLagrangianMorphing located in "/path/to/RooLagrangianMorphing", call the following command:

    ./compile source/file.cxx /path/to/RooLagrangianMorphing

Execute your script:

    cd Lesson1
    ./morphing
    python morphing.py

Gratulations! You used the RooLagrangianMorphing successfully for the first time. In directory 'Lesson1' a plot called 'plot.pdf' has been created, if you want to have more information what it shows, look at the 'Click to show detailed explanations' -section above.
### Playground

In our naming scheme, we clearly differentiate between input samples ("s1","s2","s3") and validation samples ("v0", "v1").
   * Have a look at the attached txt file 'ggfhzz4l_2d.txt'. Can you identify the SM sample?
   * What do you expect to happen if you morph to one of the input samples? Try it by changing the call of "setParameters" accordingly!
   * The validation samples contain all the infomation to be used as input samples for the morphing instead. Try swapping the roles of validation and input samples!
   * The example we provided uses exactly three input samples. What does the code do when you give one input sample too few or too many in the list?
   * At its core, the morphing relies on a matrix inversion - providing two identical input samples is the same as having two identical lines in the matrix, which makes the matrix singular. How does the code handle this?
   * We have prepared an additional input called "ggfhzz4l_2d_bad.root" that uses a different set of input samples. What happens if you use that instead?

As you have seen, 'bad input' means that the morphing is still working, but that the statistical error is very large. This can happen if the configuration of your input samples is not chosen very well. In the case of "ggfhzz4l_2d_bad.root", our input samples only cover the positiv kAzz space and are furthermore by intention set, so that the statistical error of the morphing is large.

Lesson 2: Morphing with 3 Parameters
---

In the previous lesson, you learned the basics of how to use the morphing. However, the example that we provided was very simplistic, using only three input samples, which corresponds to only two parameters modeled by the morphing: kSM and kAzz. This is the simplest non-trivial case. This is the reason why the input files you have been using so far have had "2d" in their file name.

In this lesson, you can extend the morphing to model an additional input parameter. Also, this time, you will be using VBF Higgs production with decay to two W bosons. You can find the corresponding input file in the same directory as the ones you used before, only this one is named "vbfhwwlvlv_3d.root". In your RooLagrangianMorphingTutorial create a new directory for Lesson2 and copy the morphing script over from Lesson1:

    mkdir Lesson2
    cp Lesson1/morphing.* Lesson2/.

As before, you can compile your C++ script via:

    ./compile Lesson2/morphing.cxx 

Try if you can modify the source code from the previous lesson such that it will do the job!

A hint for those who want to give it a try without looking at the detailed instructions: The identifier and inputfilename has to change now, and also the sample names and observable names will be different. For this a look into the vbfWW input file might be helpful.


First, you will of course need to change the name of the input file accordingly to use the new inputs.

You will also need to change the list of input samples to match the names from the new file. In order to spare you the hassle of opening the ROOT file and re-typing the names, here they are:

    "kAwwkHwwkSM0","kAwwkHwwkSM1","kAwwkHwwkSM10","kAwwkHwwkSM11","kAwwkHwwkSM12","kAwwkHwwkSM13","kAwwkHwwkSM2","kAwwkHwwkSM3","kAwwkHwwkSM4","kAwwkHwwkSM5","kAwwkHwwkSM6","kAwwkHwwkSM7","kAwwkHwwkSM8","kAwwkHwwkSM9","kSM0"

Since the histograms in the new input file are named differently, you will also need to change the name of your observable. We suggest you use "twoSelJets/dphijj" for a start.

Now, you have supplied the code with all the new inputs, but you will also need to instruct it about the different physics process. Luckily, there is again a pre-implemented specialized class called "RooHCvbfWWMorphFunc", that already knows about the vertex structure of this process. So all you really need to do is change the call to the constructor of your morphing function. You will later learn how to implement the morphing for completely new physics processes and create new specialized classes like this one for the process of your choice.

If you get stuck or bored while editing the code, you can find a ready-made solution <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson2/" target="_blank" title="Lesson 2">here</a>.

### Playground
   * We did not say which parameters are modeled by the morphing in this example. Can you figure out which ones just from looking at the "param_card" histograms?
   * This lesson needs more input samples than the previous one (15 as compared to 3). This has two reasons: One is the higher dimensionality, the other is the fact that the vertex structure of the VBF Higgs production with decay to vector bosons is different from the same decay in the ggF production mode. A detailed explanation of this can be found in our <a href="http://cds.cern.ch/record/2066980/files/ATL-PHYS-PUB-2015-047.pdf?version=1" target="_blank" title="ATL-PHYS-PUB-2015-047">note</a> (p. 8f, formulae 25ff). Use these formulae to check our calculations of the numbers of input samples. Do you also arrive at the numbers 3 for lesson 1 and 15 for lesson 2?
   * This input dataset conains a rather large list of validation samples: "v0","v1","v2","v3","v4","v5","v6","v7","v8","v9". You can again try replacing some of your inputs with these. Is every combination possible, or are there combinations which don't work?

Lesson 3: Customized Physics Processes
---

In this lesson, you will learn how to setup and configure the morphing for use with your own physics process. For the time being, we are still using the same scenario as in the previous lesson, but this time, we will not use the pre-made RooHCvbfWWMorphFunc! However, we will focus on processes with only two vertices for now, one of which we will label as the "production vertex", the other as the "decay vertex".

First, prepare again a directory structure for Lesson3 inside the RooLagrangianMorphingTutorial directory:

    mkdir Lesson3
    cp Lesson2/morphing.* Lesson3/.

As before, you can compile your C++ script after any changes via:

    ./compile Lesson3/morphing.cxx 

The RooLagrangianMorphing class provides a constructor to which you can pass two instances of RooArgSet that contains the couplings appearing in the individual vertices. In principle, these couplings can be any RooFit class that represents an (either dependant or independent) real number.

In order to be compliant with the <a href="http://arxiv.org/abs/1306.6464" target="_blank" title="arxiv">Higgs Characterization model</a>, which we will be using throughout this tutorial, we will create these couplings as functions of the input parameters of the model, but this is not a restriction of the implementation - rather a choice to be made depending on the model you are using.
   * Decide which input parameters you need and create an instances of RooRealVar for each of them. In the case at hand, you will need "Lambda" and "cosa" (which will be constant and set to the values of<i> Lambda=1000</i> and _cosa=1./sqrt(2)_ respectively) as well as kSM, kHww and kAww.
   * Create the couplings as instances of RooFormulaVar as dependants of your input parameters. The following list might help:
      * gSM: cosa*kSM
      * gHww: cosa*kHww/Lambda
      * gAww: sqrt(1-(cosa*cosa))*kAww/Lambda
   * Create two RooArgSets, one for each vertex.
   * Add the couplings to each vertex at which they appear.
   * Call a constructor of RooLagrangianMorphing, passing over the two lists that you have created: <br />
      * <pre>RooLagrangianMorphing(name,title,infilename,observable,prodCouplings,decCouplings,inputs);
   * And for C++ users in more detail, in order to know if pointers or no pointers are expected:
      * <pre>RooLagrangianMorphing(const char *name, const char *title, const char* infilename, const char* observable, const RooAbsCollection& prodCouplings, const RooAbsCollection& decCouplings, const RooArgList& folders);</pre>
   * And don't forget to add at the beginning of your script header via #include "XY.h" of additional classes you are using!

If you are stuck, you can always check for the <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson3/" target="_blank" title="Lesson 3">solution</a>.

Please note how this time, there is an <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson3/morphing.withClass.cxx" target="_blank" title="Lesson 3 (alternative)">additional C++ solution</a> available, which shows you how you can create your own specialized class to encapsulate the physics process you are interested in. As you can see, all you need to do is overwrite the "makeCouplings" method to setup your couplings accordingly. Unfortunately, creating your own derived class is not possible in python due to technical limitations.

_Remark: Please note how there is only one instance of each input parameter at any given time. The dependant couplings might exist in several copies or might be unique as well, depending on your implementation choice. However, if you ever end up with multiple copies of the same independent input parameter, this will confuse the tool a lot and lead to a wide variety of problems, from crashes to silently wrong results. Never create several independent parameters with the same name!_

### Playground
   * As you have now seen, the model includes additional parameters Lambda and cosa that we have not been using so far. What happens when you change their values? How does the resulting distribution change? Are the results still physical? Why should they (not)?
   * Have a look at the header file of the RooLagrangianMorphing (which is located in <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/blob/master/RooLagrangianMorphing/RooLagrangianMorphing.h">here</a>) and search for the implementations of the pre-made classes you have been using earlier. Does this look any different from what you did?

Lesson 4: Fitting
---

In this lesson, you will learn how to use the RooLagrangianMorphing to perform a fit to some pseudodata.

As you have already seen, the morphing function mapping the parameter values to output distributions is very fast - much faster than any generator would be able to generate events! The reason for this is that all the complicated physics are pre-calculated and contained in the input histograms, of which the morphing only builds a linear combination. In fact, the computation of the morphing result is so quick that it effectively forms a contuinous model. It is even feasible to perform iterative optimizations (e.g. a likelihood fit) on the morphing function, which you will explore in this lesson.

In order for this to work, we of course first need some pseudodata. For this, you can use one of the validation samples. This is especially useful because it will allow you to compare the nominal values of the parameters used in that sample with the ones that you will obtain from your fit. to obtain it, you can use the following line in C++

    RooDataHist* target = RooLagrangianMorphing::makeDataHistogram(validation,morphfunc->getObservable(),"validation");

or, similarly, in python,

    target = RooLagrangianMorphing.makeDataHistogram(validation,morphfunc.getObservable(),"validation")

The following hints will help you get the fit setup quickly:
   * Since the fit has multiple parameters and the likelihood is not always well-behaved, it helps to start in the proximity of your known (or expected) values.
   * Start off by calling "setParameters" to set the parameters of the validation sample you want to use as pseudodata.
   * In order to give the fit something to do, you probably want to vary the parameters just a bit. You can do this by either manually setting their values using the "setParameter" method, or by adding a random fluctuation using the "randomizeParameters" method, which takes the number of standard deviations by which to randomize as an argument. By default, the standard deviations of the input paremeters are set to 0.001.
   * You can also use the "setParameterConstant" method to freeze individual parameters before fitting.
For the actual fit, you just need to retrieve the internal probability density function by calling the "getPdf" method, on which you can then call the standard Roofit method "fitTo". Afterwards, you can all the RooLagrangianMorphing method "printParameters" to print to console the results of the fit.

You should also call the "createTH1" method one more time after performing the fit and plot the distribution obtained from the fit on the same canvas. Ideally, all three distributions - the validation, the result of morphing to the nominal parameter values and the fit result should be compatible!

As always, you can look at the <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson4/" target="_blank" title="Lesson 4">solution</a> if you are stuck.

### Playground
   * Use the "printParameters" method right after setting the parameters of your validation sample and compare the numbers to those obtained after the fit. Did you achieve closure?
   * Like most complex fit models, it's easy to choose parameter settings in which the fit does either not converge or not find the right minimum. Try to get a feeling for how close to the target point you need to choose your initial values to make the fit converge to the correct minimum!
   * You will quickly notice that the fit is much more sensitive to some parameters than it is to others. Which parameters are especially unstable?

Lesson 5: Matrices
---

As pointed out earlier, the morphing at its heart relies on a matrix inversion. In this lesson, you will learn how to work with the matrix that the RooLagrangianMorphing uses internally.

The RooLagrangianMorphing has a method called "writeCoefficients", which allows you to write out the (already inverted) matrix with the morphing coefficients to a text file (the name of which you can give as an argument). Execute this code and have a look at the text file.

You can also read this matrix back into the RooLagrangianMorphing, using the sister method "useCoefficents", which again takes the filename as an argument.

_Remark: You can only call the useCoefficients method directly after instantiating the RooLagrangianMorphing, before calling any other methods. Otherwise, the first other method call will trigger the calculation of inverse matrix, which will prohibit you from overwriting it with the contents of your text file later._

As always, you can again check the <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson5/" target="_blank" title="Lesson 5">solution</a> if you are stuck. This time, the solution is split into a very simplistic script that only creates and writes out the morphing matrix, and a standard morphing script that will read back that file.

### Playground
   * Have a look at the text file containing the morphing matrix. Pay attention to the order of magnitude of the entries.
   * Re-Use your old solution from lesson 1 and compare the matrices obtained from the two different sets of input files. Looking at the matrix, can you immediately spot what is the main difference between the two sets of input files?
   * Note how the dimension _n_ of the (square) matrix is equal to the number of input samples. In general, inversion of an _n x n_ matrix takes approximately _n³_ computational steps. Now think of a model that contains 10 or 20 parameters. Can you see now why it might be useful to be able to read in and write out the already inverted matrix using a file?
   * The inversion of matrices is prone to numerical instabilities. A good measure for the size of these instabilities is the <a href="https://en.wikipedia.org/wiki/Condition_number" target="_blank" title="Wikipedia">condition number</a> of the matrix. As a rule of thumb, the logarithm of the condition number tells you how many digits of accuracy you lose. You can obtain the condition number from the RooLagrangianMorphing by using the "getCondition" method. Compare the condition numbers of the matrices for the three cases you have been looking at (ggfhzz4l_2d.root, ggfhzz4l_2d_bad.root and vbfhwwlvlv_3d.root). What do you observe?
   * The RooLagrangianMorphing by default uses the standard ROOT implementation of TMatrixD for the matrix inversion, that is, employing double precision floating point arithmetics. This is sufficient for the simple cases we are using in this tutorial. However, for larger models and especially ill-conditioned matrices, higher precision is needed. To accomodate this, the RooLagrangianMorphing can also be compiled against the C++ <i>boost </i>libraries<i> ublas</i> and _multiprecision_, which provides higher precision floating types and efficient matrix algebra using LU decomposition. Look again at the text file that you have been writing and re-reading. Can you spot whether the implementation you have been using was using double precision or the more precise _ublas_ implementation? _Hint: Numbers have around 16 significant digits in their decimal representation with double precision._
   * As you probably have guessed, the RooLagrangianMorphing provided under the path ending with "default" is compiled with standard ROOT double precision, whereas there is another instance of the library ending with "ublas", which provides higher precision numerics. Compare the matrices - can you see the difference?

Lesson 6: Number of expected events
---

Sometimes, one is not only interested in comparing different physics distributions, but wants to plot a quantity - for example, the number of expected events - as a function of one of the input parameters. This lesson will show you how to do exactly that. The solution will show an example using ggfZZ, but it can of course also be done with vbfWW.

The RooLagrangianMorphing has a method called "expectedEvents" (and, correspondingly, "expectedUncertainty") that will yield the number of expected events (integral of the histogram) in the current configuration.

Setup your directory structure as before and write a small loop that manually scans through a parameter using the "setParameter" method to evaluate a couple of discrete points, retrieve the number of expected events (with uncertainty) and plot them as a TGraphErrors. In the provided ggfZZ input file a mixture of kSM with kAzz is modelled. So here it might be interesting to vary for example kAzz in a range of [-20,20]. For vbfWW a mixture of kSM with kAww and kHww is modelled, so here you could vary kAww and kHww.

As always, you can check the <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson6/" target="_blank" title="Lesson 6">solution</a> if you are stuck.

### Playground
   * How many points do you need to obtain a smooth line for your graph? How long does the RooLagrangianMorphing take to calculate? Make a guess: If you wanted to make the same plot without using morphing, instead running a generator for every point - how long would your code run?
   * Try making a 2D-Plot that shows the number of expected events as a function of two different parameters. How many points do you need for that?
   * Observe how also the uncertainty on the number of expected events changes as a function of the parameters. Plot vertical lines at the coordinates of your input samples, and watch how the uncertainty band behaves at these points - what do you observe? Are you surprised?

Lesson 7: Workspaces
---

In this lesson, you will learn how to use the RooLagrangianMorphing in conjunction with RooFit workspaces.

Create a RooWorkspace. Now, we will use the workspace factory functionality to create an instance of RooLagrangianMorphing in the workspace. Build a string that looks like this:

    Roo[type]Func::[name]('[infile.root]','[observable]',{'[input1]','[input2]',...})

where each occurrence of brackets <i>[...] </i>needs to be replaced with the corresponding piece of text. Now, pass this string to the "RooWorkspace::factory(str)" method.

Save the workspace to a ROOT file.

In a different piece of code, re-open the file and get back your RooLagrangianMorphing using the "RooWorkspace::obj(name)" method.

Check if the RooLagrangianMorphing you obtained from the workspace behaves as you would expect.

As always, if you are stuck, you can consult the <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson7/" target="_blank" title="Lesson 7">solution</a>, which is again split in two separate scripts - one to create the workspace and write it to a ROOT file, the other to obtain the workspace back to a file and extract the RooLagrangianMorphing from it.

Playground
-----
   * You have learned about the two different implementations using the default TMatrixD matrix algebra and the _ublas multiprecision_ implementation in lesson 5. What would happen if you write a workspace in one version and read it with the other? What do you expect? Try it!<i><br /></i>
   * In lesson 3, you got to know how to create your own specialized class of RooLagrangianMorphing. Can you also instantiate that one with the workspace factory?

Lesson 8: Plotting sample weights
---

In this lesson, we will investigate how exactly the morphing works and how the weights assigned to invidiual samples in the linear combination changes as a function of the parameters.

The RooLagrangianMorphing class has a method called "getSampleWeight" that takes as an argument the name of a sample and returns a pointer to an instance of RooAbsReal that reflects the weight of this sample. You can now scan through a parameter (like you did in lesson 6) and instead of the number of expected events, plot the weight that a particular sample is assigned in the linear combination that is the morphing.

Plot the weights of all samples as a function of one of the input parameters on the same canvas.

As always, if you are stuck, you can consult the <a href="https://gitlab.cern.ch/cburgard/RooLagrangianMorphing/tree/master/share/tutorial/lesson8" target="_blank" title="Lesson 8">solution</a>.

Playground
---
   * Draw markers (vertical lines) at the points where your input samples are located. How do the weights behave at these points?
   * Compare the weights of the samples with the uncertainty band obtained from the morphing in lesson 6. Does this behavior surprise you?<hr />

