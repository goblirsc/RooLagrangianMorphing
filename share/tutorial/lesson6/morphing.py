#!/bin/env python
import argparse
import os

def main():
  import ROOT 

  # define process identifier, input file and observable
  identifier = "ggfZZ" # avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  infilename = "input/ggfhzz4l_2d.root" # give the input file name here
  observable = "base/phi" # name of the observable to be used (histogram name)

  # these are the names of the input samples
  samplelist = ["s1","s2","s3"]
  # these are the validation samples: "v0","v1"

  # push all the input samples in a RooArgList
  inputs = ROOT.RooArgList()
  # we need the additional list "inputnames" to prevent the python garbage collector from deleting the RooStringVars
  inputnames = []
  for sample in samplelist:
    v = ROOT.RooStringVar(sample,sample,sample)
    inputnames.append(v)
    inputs.add(v)

  # setup predefined morphfunc by hand
  morphfunc = ROOT.RooHCggfZZMorphPdf(identifier,identifier,infilename,observable,inputs)

  standardmodel = "s1"
  morphfunc.setParameters(standardmodel)

  c = ROOT.TCanvas("plot")
  c.cd()

  g = ROOT.TGraphErrors()
  i=0
  for val in [-5.,-4.,-3.,-2.,-1.,0.,1.,2.,3.,4.,5.]:
    morphfunc.setParameter("kAzz",val)
    g.SetPoint     (i,val,morphfunc.expectedEvents())
    g.SetPointError(i,0.,morphfunc.expectedUncertainty())
    i = i+1
  g.SetMarkerStyle(20)
  g.Draw("AEP")
  g.GetXaxis().SetTitle("#kappa_{AZZ}")
  g.GetYaxis().SetTitle("expected events")
  c.SaveAs("plot.pdf")

if __name__ == "__main__":
  # some argument parsing to provide the path to the RooLagrangianMorphFunc
  parser = argparse.ArgumentParser("morphing example")
  args = parser.parse_args()
  # load all required libraries
  from ROOT import gSystem
  gSystem.Load("libRooFit")
  gSystem.Load("libRooLagrangianMorphing.so")
  # call the main function
  main()
