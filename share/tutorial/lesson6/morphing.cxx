// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooStringVar.h"

// ROOT
#include "TFile.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TFolder.h"
#include "TAxis.h"

int main(int argc, const char* argv[]){

  // define process identifier, input file and observable
  std::string identifier("ggfZZ"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/ggfhzz4l_2d.root"); // give the input file name here
  std::string observable("base/phi"); // name of the observable to be used (histogram name)

  // these are the names of the input samples
  std::vector<std::string> samplelist = {"s1","s2","s3"};
  // these are the validation samples: "v0","v1"

  // push all the input samples in a RooArgList
  RooArgList inputs;
  for(auto const& sample: samplelist) {
    RooStringVar* v = new RooStringVar(sample.c_str(),sample.c_str(),sample.c_str());
    inputs.add(*v);
  }

  // setup predefined morphfunc by hand
  RooLagrangianMorphPdf* morphfunc = new RooHCggfZZMorphPdf(identifier.c_str(),identifier.c_str(),infilename.c_str(),observable.c_str(),inputs);
  std::string standardmodel("s1");
  morphfunc->setParameters(standardmodel.c_str());

  TCanvas* c = new TCanvas("plot");
  c->cd();

  TGraphErrors* g = new TGraphErrors();
  size_t i=0;
  for(auto val:{-5.,-4.,-3.,-2.,-1.,0.,1.,2.,3.,4.,5.}){
    morphfunc->setParameter("kAzz",val);
    g->SetPoint     (i,val,morphfunc->expectedEvents());
    g->SetPointError(i,0.,morphfunc->expectedUncertainty());
    i++;
  }
  g->SetMarkerStyle(20);
  g->Draw("AEP");
  g->GetXaxis()->SetTitle("#kappa_{AZZ}");
  g->GetYaxis()->SetTitle("expected events");
  c->SaveAs("plot.pdf");
  return 0;
}
