def dict_to_hist(d,name):
  import ROOT
  n = len(d)
  hist = ROOT.TH1F(name,name,n,0,n)
  hist.SetDirectory(0)
  i = 1
  for k in sorted(d.keys()):
    hist.GetXaxis().SetBinLabel(i,k)
    hist.SetBinContent(i,d[k])
    i = i+1

  return hist
